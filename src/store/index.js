import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        loadedMeetups: [
            // {
            //     title: 'meetup in New York',
            //     location: 'New York',
            //     imgUrl: 'https://westcordhotels.com/wp-content/uploads/2017/11/hotel-new-york-rotterdam-01-1024x480.jpg',
            //     id: '1',
            //     date: '28.08.2018',
            //     description: `Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.`
            // },
            // {
            //     title: 'meetup in Paris',
            //     location: 'Paris',
            //     imgUrl: 'https://euroswissglobal.itravelsoftware.com/slikeOpisiJedinica/paris-xmas-1024x480.jpg',
            //     id: '2',
            //     date: '29.08.2018',
            //     description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
            // }
        ],
        user: null,
        error: null,
        loading: false
    },
    mutations: {
        setLoadedMeetups(state, payload) {
            state.loadedMeetups = payload
        },
        createMeetup(state, payload) {
            state.loadedMeetups.push(payload)
        },
        setUser(state, payload) {
            state.user = payload
        },
        setLoading(state, payload) {
            state.loading = payload 
        },
        setError(state, payload) {
            state.error = payload 
        },
        clearError(state) {
            state.error = null 
        }
    },
    actions: {
        loadedMeetups({commit}) {
          commit('setLoading', true);
          firebase.database().ref('meetups').once('value')
            .then((data) => {
              const meetups = []
              const obj = data.val()
              for (let key in obj) {
                meetups.push({
                  id: key,
                  title: obj[key].title,
                  location: obj[key].location,
                  imgUrl: obj[key].imgUrl,
                  description: obj[key].description,
                  date: obj[key].date
                })
              }
              commit('setLoading', false);
              commit('setLoadedMeetups', meetups)
            })
            .catch((error) => {
              commit('setLoading', false);
              console.log(error);
            })
        },
        createMeetup({commit}, payload) {
            const meetup = {
                title: payload.title,
                location: payload.location,
                imgUrl: '',
                description: payload.description,
                date: payload.date.toLocaleDateString()
            }
            // firebase
          let imageUrl = null
            let key = null
            firebase.database().ref('meetups').push(meetup)
                .then((data) => {
                  key = data.key
                  return key
                })
                .then(key => {

                  const filename = payload.img.name
                  const ext = filename.slice(filename.lastIndexOf('.')+1)
                  return firebase.storage().ref('meetups/' + key + '.' + ext).put(payload.img)
                })
                .then(fileData => {
                  console.log(fileData.metadata.downloadURL);
                  imageUrl = fileData.metadata.fullPath

                  return firebase.database().ref('meetups').child(key).update({imgUrl: imageUrl})
                })
                .then(() => {
                  commit('createMeetup', {
                    ...meetup,
                    imgUrl: imageUrl,
                    id: key
                  })
                })
                .catch((error) => {
                    console.log(error)
                    commit('setError', error)
                })

        },
        signUserUp({commit}, payload) {
            commit('setLoading', true);
            commit('clearError');
            firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
                .then(
                    user => {
                        const newUser = {
                            id: user.user.uid,
                            registeredMeetups: []
                        }
                        commit('setLoading', false);
                        commit('setUser', newUser)
                    }
                )
                .catch(
                    error => {
                        commit('setLoading', false);
                        commit('setError', error);
                    }
                )
        },
        signUserIn({commit}, payload) {
            commit('setLoading', true);
            commit('clearError');
            firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
                .then(
                    data => {
                        const userData = {
                            id: data.user.uid,
                            email: data.user.email,
                            registeredMeetups: []
                        }
                        commit('setLoading', false);
                        commit('setUser', userData);
                    }
                )
                .catch(
                    error => {   
                        commit('setError', error);                     
                        console.log(error);
                        commit('setLoading', false);
                    }
                );
        },
        clearError({commit}) {
            commit('clearError');
        }
    },
    getters: {
        loading(state) {
          return state.loading
        },
        loadedMeetups(state) {
            return state.loadedMeetups.sort( (meetupA, meetupB) => {
                return meetupA.date > meetupB.date
            } )
        },
        featuredMeetups(state, getters) {
            return getters.loadedMeetups.slice(0, 5)
        },
        loadedMeetup(state) {
            return meetupId => {
                return state.loadedMeetups.find( (meetup) => {
                    return meetup.id === meetupId
                } )
            }
        },
        user(state) {
            return state.user   
        },
        error(state) {
            return state.error
        }
    }
})
