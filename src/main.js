import Vue from 'vue'
import { store } from './store'
import App from './App.vue'
import Router from 'vue-router'

// bootstrap
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Bootstrap from 'bootstrap-vue'
import { Navbar, Layout, Button } from 'bootstrap-vue/es/components'

Vue.use(Navbar)
Vue.use(Layout)
Vue.use(Button)

import Home from './components/Home'
import Meetups from './components/Meetup/Meetups'
import CreateMeetup from './components/Meetup/CreateMeetup'
import Meetup from './components/Meetup/Meetup'
import Profile from './components/User/Profile'
import SignIn from './components/User/SignIn'
import SignUp from './components/User/SignUp'
import AlertError from './components/Shared/AlertError'
import Loading from './components/Shared/Loading'


Vue.use(Router)

Vue.component(Home)
Vue.component(Meetups)
Vue.component(CreateMeetup)
Vue.component(Profile)
Vue.component(SignIn)
Vue.component(SignUp)
Vue.component('alert-error', AlertError)
Vue.component('loading', Loading)

// axios
import Axios from 'axios'

var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/meetups',
      name: 'Meetups',
      component: Meetups
    },
    {
      path: '/new',
      name: 'CreateMeetup',
      component: CreateMeetup
    },
    {
      path: '/meetups/:id',
      name: 'Meetup',
      props: true,
      component: Meetup
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/sign-in',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    }
  ]
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.dispatch('loadedMeetups')
  }
})
